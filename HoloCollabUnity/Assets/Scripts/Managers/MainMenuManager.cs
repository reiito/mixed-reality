﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager mainMenuManagerInstance;

    public GameObject sceneSelectObj;

    bool onMenu;
    public bool OnMenu{ get { return onMenu; } }

    bool onSceneSelect;

    GameManager gameManager;

    Animator animator;

    private void Awake()
    {
        mainMenuManagerInstance = this;
        animator = GetComponent<Animator>();
        sceneSelectObj.SetActive(false);
        gameObject.SetActive(false);
    }

    private void Start()
    {
        gameManager = GameManager.gameManagerInstance;
    }

    /// <summary>
    /// Owl click behaviour: position, rotation and menu settings
    /// </summary>
    /// <param name="newPosition"></param>
    public void OnOwlClick(Vector3 newPosition)
    {
        if (onMenu)
        {
            TurnOffMenu();
        }
        else
        {
            // UI Position
            transform.position = newPosition;
            // Adust position on click
            transform.position = Vector3.MoveTowards(transform.position, Camera.main.transform.position, 0.2f); //forward distance

            // Rotate UI to look at camera
            Vector3 v = Camera.main.transform.position - transform.position;
            v.x = v.z = 0.0f;
            transform.LookAt(Camera.main.transform.position - v);
            transform.Rotate(0, 180, 0);

            onMenu = true;
            gameObject.SetActive(onMenu);
            animator.SetTrigger("PopIn");
        }
    }

    public void TurnOffMenu()
    {
        onMenu = false;
        animator.SetTrigger("PopOut"); // 'MainMenuPopOut.cs' disables the menu
    }


    public void HideMenu()
    {
        if (onSceneSelect)
        {
            ToggleSceneSelect();
        }
        gameObject.SetActive(onMenu);
    }

    #region Buttons

    int prevIndex = 0;
    public void OnPreviousMeshButton()
    {
        OnSwapSceneButton(prevIndex);
    }

    public void ToggleSceneSelect()
    {
        onSceneSelect = !onSceneSelect;
        sceneSelectObj.SetActive(onSceneSelect);
    }

    /// <summary>
    /// Given an index swap the mesh index and store the previous one
    /// </summary>
    /// <param name="meshIndex"></param>
    public void OnSwapSceneButton(int meshIndex)
    {
        if (gameManager.CurrentMeshIndex == meshIndex)
            return;

        prevIndex = gameManager.CurrentMeshIndex;

        gameManager.CurrentMeshIndex = meshIndex;

        if (gameManager.CurrentMesh)
        {
            gameManager.meshAnimator.SetTrigger("SwitchOut");
        }
        else
        {
            gameManager.SwapMesh();
        }

        sceneSelectObj.SetActive(false);
    }

    #endregion
}
