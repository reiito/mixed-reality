﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionMode
{
    NONE,
    PIN,
    MOVE,
    ROTATE,
    SCALE
}

[System.Serializable]
public struct MeshParent
{
    public GameObject mesh;
    public GameObject[] layers;
}

public class GameManager : MonoBehaviour
{
    public static GameManager gameManagerInstance;

    public MeshParent[] meshes;
    public Animator meshAnimator;
    public GameObject sceneModel;
    public GameObject pinPrefab;

    public InteractionMode mode;

    public float rotationSensitivity = 10f;
    public float scaleSensitivity = 5f;

    NetworkManager networkManager;
    PinMenuManager pinMenuManager;
    MainMenuManager mainMenuManager;

    List<Pin> scenePins;
    public List<Pin> ScenePins { get { return scenePins; } }

    Pin selectedPin;
    public Pin SelectedPin { get { return selectedPin; } }

    GameObject currentMesh;
    public GameObject CurrentMesh { get { return currentMesh; } set { currentMesh = value; } }

    int currentMeshIndex = -1;
    public int CurrentMeshIndex { get { return currentMeshIndex; } set { currentMeshIndex = value; } }

    int currentLayerIndex;
    public int CurrentLayerIndex { get { return currentLayerIndex; } set { currentLayerIndex = value; } }

    float pinScale = 0.09f;

    bool canUpdate;
    public bool CanUpdate { get { return canUpdate; } set { canUpdate = value; } }

    private void Awake()
    {
        gameManagerInstance = this;

        scenePins = new List<Pin>();

        foreach (MeshParent mp in meshes)
        {
            foreach (GameObject go in mp.layers)
            {
                go.SetActive(false);
            }
            mp.mesh.SetActive(false);
        }

        canUpdate = true;
    }

    private void Start()
    {
        networkManager = NetworkManager.networkManagerInstance;
        pinMenuManager = PinMenuManager.pinMenuManagerInstance;
        mainMenuManager = MainMenuManager.mainMenuManagerInstance;
    }

    /// <summary>
    /// Determine what's been clicked/pinched and the relavant action to take.
    /// </summary>
    public void GestureBehaviour()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo, 20.0f, Physics.DefaultRaycastLayers))
        {
            if (!pinMenuManager.OnMenu)
            {
                if (hitInfo.collider.tag == "Pin")
                {
                    selectedPin = hitInfo.collider.gameObject.GetComponent<Pin>();
                    pinMenuManager.SetVisibilityToggleButton(selectedPin.HasComment());
                    pinMenuManager.OnPinClick(hitInfo.point, true);
                    canUpdate = false;
                }
                else if (hitInfo.collider.tag == "Model")
                {
                    PlacePin(hitInfo.point);
                }
            }

            if (hitInfo.collider.tag == "OwlBot")
            {
                mainMenuManager.OnOwlClick(hitInfo.point);
            }
        }
    }

    /// <summary>
    /// Swap the mesh depending on the changed index and load the relevant pins onto it
    /// </summary>
    public void SwapMesh()
    {
        canUpdate = false;

        if (currentMesh)
        {
            currentMesh.SetActive(false);
        }

        currentLayerIndex = 0;

        currentMesh = meshes[currentMeshIndex].layers[currentLayerIndex];

        NetworkManager.networkManagerInstance.request.setMeshName(currentMesh.name);

        meshes[currentMeshIndex].mesh.SetActive(true);
        currentMesh.SetActive(true);

        meshAnimator.SetTrigger("SwitchIn");

        canUpdate = true;
    }

    /// <summary>
    /// Work the same way as the mesh swapping but without the animations
    /// </summary>
    public void SwapLayer()
    {
        canUpdate = false;

        if (currentLayerIndex < 0)
        {
            currentLayerIndex = 0;
            return;
        }
        else if (currentLayerIndex > meshes[currentMeshIndex].layers.Length - 1)
        {
            currentLayerIndex = meshes[currentMeshIndex].layers.Length - 1;
            return;
        }

        if (currentMesh)
        {
            currentMesh.SetActive(false);
        }
        currentMesh = meshes[currentMeshIndex].layers[currentLayerIndex];

        currentMesh.SetActive(true);

        NetworkManager.networkManagerInstance.request.setMeshName(currentMesh.name);

        networkManager.LoadPinData();

        canUpdate = true;
    }

    #region Pinning

    /// <summary>
    /// Place pin on a given vector position facing the camera and add it to the scene's pin tracker list
    /// </summary>
    /// <param name="point"></param>
    void PlacePin(Vector3 newPosition)
    {
        canUpdate = false;

        GameObject newPinObj = Instantiate(pinPrefab, currentMesh.transform);

        // Pin Position
        newPinObj.transform.position = newPosition;
        // Face camera
        Vector3 relativePos = Camera.main.transform.position - newPinObj.transform.position;
        newPinObj.transform.rotation = Quaternion.LookRotation(relativePos);

        Pin newPin = newPinObj.GetComponent<Pin>();

        newPin.SetGlobalScale(pinScale);

        newPin.PinDetails = new PinDetails(newPinObj.transform);

        scenePins.Add(newPin);
        networkManager.SavePinData(scenePins);

        canUpdate = true;
    }

    /// <summary>
    /// Load behaviour for pin placing
    /// </summary>
    /// <param name="newPosition"></param>
    /// <param name="newRotation"></param>
    /// <param name="newComment"></param>
    public void PlacePin(Vector3 newPosition, Quaternion newRotation, string newComment, bool showComment)
    {
        GameObject newPinObj = Instantiate(pinPrefab, newPosition, newRotation, currentMesh.transform);
        Pin newPin = newPinObj.GetComponent<Pin>();
        newPin.SetGlobalScale(pinScale);
        newPin.PinDetails = new PinDetails(newPinObj.transform);
        if (newComment != "" && newComment != null)
            newPin.AddComment(newComment);

        if (!showComment)
        {
            newPin.ToggleCommentMesh();
        }

        scenePins.Add(newPin);
    }

    /// <summary>
    /// Delete pin from list and world
    /// </summary>
    public void RemovePin()
    {
        canUpdate = false;
        scenePins.Remove(selectedPin);
        Destroy(selectedPin.gameObject);
        networkManager.SavePinData(scenePins);
        canUpdate = true;
    }

    /// <summary>
    /// Clear all pins from list and world
    /// </summary>
    public void ClearPins()
    {
        foreach (Pin p in scenePins)
        {
            Destroy(p.gameObject);
        }

        scenePins.Clear();
    }

    #endregion
}
