﻿using UnityEngine;
using UnityEngine.UI;

using HoloToolkit.UI.Keyboard;

public class PinMenuManager : MonoBehaviour
{
    public static PinMenuManager pinMenuManagerInstance;

    public GameObject visibleButtonObj;

    bool onMenu;
    public bool OnMenu { get { return onMenu; } }

    GameManager gameManager;

    Animator animator;

    bool commentPress;

    private void Awake()
    {
        pinMenuManagerInstance = this;
        animator = GetComponent<Animator>();
        gameObject.SetActive(false);
    }

    private void Start()
    {
        gameManager = GameManager.gameManagerInstance;

        Keyboard.Instance.OnTextUpdated += delegate { OnKeyboardUpdate(); };
        Keyboard.Instance.OnTextSubmitted += delegate { OnKeyboardEnd(); };
        Keyboard.Instance.OnClosed += delegate { OnKeyboardClose(); };
    }

    /// <summary>
    /// Enable all required objects for menu and reposition to clicked area.
    /// </summary>
    /// <param name="newTransform"></param>
    /// <param name="pin"></param>
    public void OnPinClick(Vector3 newPosition, bool hasComment)
    {
        // UI Position
        transform.position = newPosition;
        // Adust position on click
        transform.position = Vector3.MoveTowards(transform.position, Camera.main.transform.position, 0.2f); //forward distance
        // Rotate UI to look at camera
        Vector3 v = Camera.main.transform.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(Camera.main.transform.position - v);
        transform.Rotate(0, 180, 0);

        ToggleMenu();

        animator.SetTrigger("PopIn");
    }

    public void SetVisibilityToggleButton(bool value)
    {
        visibleButtonObj.SetActive(value);
    }

    #region Buttons

    public void OnCloseButton()
    {
        animator.SetTrigger("PopOut");
    }

    /// <summary>
    /// Handles keyobard position rotationa and functionality
    /// </summary>
    public void OnCommentButton()
    {
        commentPress = true;

        Keyboard.Instance.transform.position = new Vector3(transform.position.x, transform.position.y - 0.23f, transform.position.z);
        Vector3 v = Camera.main.transform.position - transform.position;
        v.x = v.z = 0.0f;
        Keyboard.Instance.transform.LookAt(Camera.main.transform.position - v);
        Keyboard.Instance.transform.Rotate(0, 180, 0);

        Keyboard.Instance.PresentKeyboard();
        if (!gameManager.SelectedPin.commentMesh.gameObject.activeInHierarchy)
        {
            gameManager.SelectedPin.ToggleCommentMesh();
        }
        inputted = false;

        animator.SetTrigger("PopOut");
    }

    public void OnHideButton()
    {
        gameManager.SelectedPin.ToggleCommentMesh();
        NetworkManager.networkManagerInstance.SavePinData(gameManager.ScenePins);
        animator.SetTrigger("PopOut");
    }

    public void OnDeleteButton()
    {
        gameManager.RemovePin();
        animator.SetTrigger("PopOut");
    }

    #endregion

    #region Button Functionality

    /// <summary>
    /// Turn menu on and off
    /// </summary>
    public void ToggleMenu()
    {
        onMenu = !onMenu;
        gameObject.SetActive(onMenu);
        Debug.Log(commentPress);

        if (!onMenu && !commentPress)
        {
            gameManager.CanUpdate = true;
        }
    }

    #endregion

    #region Keyboard Events

    /// <summary>
    /// Fill text mesh as use is typing/speeking
    /// </summary>
    void OnKeyboardUpdate()
    {
        if (Keyboard.Instance.InputField.text != "")
            gameManager.SelectedPin.AddComment(Keyboard.Instance.InputField.text);
    }

    /// <summary>
    /// Save inputted text into text mesh
    /// </summary>
    bool inputted;
    void OnKeyboardEnd()
    {
        inputted = true;
        NetworkManager.networkManagerInstance.SavePinData(gameManager.ScenePins);
    }

    void OnKeyboardClose()
    {
        if (!inputted)
        {
            gameManager.SelectedPin.RemoveComment();
        }

        gameManager.CanUpdate = true;
        commentPress = false;
    }

    #endregion
}

