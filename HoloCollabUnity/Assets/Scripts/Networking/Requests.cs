﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

//getAuthToken() returns the token to be stored in a OAuth2 object
//retrieveMesh() returns the pins pinned on the model to be stored in a Mesh_Object object
//saveMesh() sends the Mesh_Object object back to the server

public class Requests : MonoBehaviour
{
	public Text displayText;
	public OAuth2 token;
	public MeshDetails mesh = new MeshDetails("Person");
	private string username = "name@name.com";
	private string password = "123456";
	private string baseUrl = "http://mr.blackseal.melbourne/api/";

	private void Awake()
	{
        // TODO get the Mesh name on creation
		getAuthToken();
    }

	public void getAuthToken()
	{
		// Action to reach at the endpoint
		string action = "auth/login";

        // DEBUG
        //Debug.Log ("Requesting auth token");

        // Code to request new token
		string json = "{\"email\":\"" + username + "\", \"password\":\"" + password + "\", \"remember_me\":true}";
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(json);
        UnityWebRequest www = UnityWebRequest.Put(baseUrl + action, bytes);
        www.chunkedTransfer = false;
        www.method = "POST";
        www.SetRequestHeader("Content-Type", "application/json");
        www.SetRequestHeader("X-Requested-With", "XMLHttpRequest");
		StartCoroutine(WaitForRequest(www));
	}

	IEnumerator WaitForRequest(UnityWebRequest data)
	{		
		yield return data.SendWebRequest();

        if (data.isNetworkError || data.isHttpError)
        {
            Debug.Log(data.error);
        }
		// When data is returned, JSON decode the information into the OAuth2 object type
		token = new OAuth2();
        
        // Use the inbuilt JSON from Newtonsoft to get the token data
        token = Newtonsoft.Json.JsonConvert.DeserializeObject<OAuth2>(data.downloadHandler.text);
    }

	public PinDetails[] getMesh()
	{
		var url = "http://mr.blackseal.melbourne/api/mesh/" + mesh.name;
        //Debug.Log("###### Token type" + token.token_type + " Token " + token.access_token);
		string payload = token.token_type + " " + token.access_token;

        // Code to request mesh
        UnityWebRequest www = UnityWebRequest.Get(url);
        //www.chunkedTransfer = false;
        //www.method = "POST";
        www.SetRequestHeader("Content-Type", "application/json");
        www.SetRequestHeader("X-Requested-With", "XMLHttpRequest");
        www.SetRequestHeader("Authorization", payload);

        // Make the request for pins upstream
        www.SendWebRequest();

        while(!www.isDone)
        {
            //Loopin' boss...
            continue;
        }

        // Fill the mesh with pin and name data
        //Debug.Log("###### WWW TEXT: " + www.downloadHandler.text );

        // Turn the JSON array of pins into Pin objects
        PinDetails[] pins = Newtonsoft.Json.JsonConvert.DeserializeObject<PinDetails[]>(www.downloadHandler.text);

        //Debug.Log("###### WWW TEXT:" + pins[0].comment + ":" );

        // Return the contents of the pins
        return pins;
    }

	public void updateMesh(string value)
	{
		// Example of what is sent upstream
		// {"Pins":[{"id":"1","pos":[2.2398137511370209e-10,0.0,1.914706826210022],"rot":[0.0,1.0,0.0,-5.848973388955514e-11],"comment":""}]}

		string action = "mesh/" + mesh.name;

        // TODO NEED TO CHECK IF TOKEN IS VALID before making request
        string payload = token.token_type + " " + token.access_token;
        // Code to request new token
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(value);
        
        UnityWebRequest www = UnityWebRequest.Put(baseUrl + action, bytes);
        www.chunkedTransfer = false;
        www.method = "POST";
        www.SetRequestHeader("Content-Type", "application/json");
        www.SetRequestHeader("X-Requested-With", "XMLHttpRequest");
        www.SetRequestHeader("Authorization", payload);

        www.SendWebRequest();
	}


    // Unofficial mesh constructor
    public void setMeshName(string name)
    {
        mesh = new MeshDetails(name);
    }
}
