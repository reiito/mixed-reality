﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MeshDetails
{

    public string id;
    public string name;
    public string pins;
    public string created_at;
    public string updated_at;

    public MeshDetails(string name)
    {
        this.name = name;
    }
}
