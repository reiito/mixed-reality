﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager networkManagerInstance;

    public float pollTime;

    public Requests request;

    GameManager gameManager;

    private void Awake()
    {
        networkManagerInstance = this;

        // Create the requests object that will handle authentication and pull/push of mesh and pin data
        request = gameObject.AddComponent<Requests>();
    }

    private void Start()
    {
        gameManager = GameManager.gameManagerInstance;
    }

    float currTime;
    private void Update()
    {
        // Debug load key TODO: detect changes to the server file to see if there have been any changes then load
        if (Input.GetKeyDown("."))
        {
            LoadPinData();
        }

        // Debug save key, TODO: detect local changes and save to server
        if (Input.GetKeyDown(","))
        {
            SavePinData(gameManager.ScenePins);
        }

        if (gameManager.CurrentMesh && currTime > pollTime && gameManager.CanUpdate)
        {
            currTime = 0;
            Debug.Log("pins loaded via polling");
            LoadPinData();
        }

        currTime += Time.deltaTime;
    }

    /// <summary>
    /// Finds all pins in scene and saves them to JSON file.
    /// </summary>
    // NOTE: very heavy if saving every local change
    // TODO: make list and add to on local change instead, then save list
    public void SavePinData(List<Pin> scenePins)
    {
        PinDetails[] pinsToSave = new PinDetails[scenePins.Count];

        for (int i = 0; i < pinsToSave.Length; i++)
        {
            pinsToSave[i] = scenePins[i].PinDetails;
        }

        // Make the JSON sting 
        //PinDetails[] pins = Newtonsoft.Json.JsonConvert.DeserializeObject<PinDetails[]>(www.downloadHandler.text);
        string pinJsonString = Newtonsoft.Json.JsonConvert.SerializeObject(pinsToSave);
        // Send to upstream
        request.updateMesh(pinJsonString);
    }

    /// <summary>
    /// Grab JSON string from server and reproduce in scene.
    /// </summary>
    // TODO: use new array to adjust existing scene list of pins and refresh details
    public void LoadPinData()
    {
        gameManager.ClearPins();

        PinDetails[] loadedPins = request.getMesh();

        for (int i = 0; i < loadedPins.Length; i++)
        {
            gameManager.PlacePin(loadedPins[i].GetPosition(), loadedPins[i].GetRotation(), loadedPins[i].comment, loadedPins[i].display);
        }
    }
}
