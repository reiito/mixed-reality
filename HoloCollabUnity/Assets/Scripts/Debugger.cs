﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ModelView
{
    public bool showModel;
    public int modelIndex;
    public int layerIndex;
}

public class Debugger : MonoBehaviour
{
    public static Debugger debuggerInstance;

    public bool debugMode;

    public InteractionMode mode;

    public ModelView modelView;

    GameManager gameManager;

    private void Awake()
    {
        debuggerInstance = this;
    }

    private void Start()
    {
        gameManager = GameManager.gameManagerInstance;

        if (debugMode)
        {
            gameManager.mode = mode;

            if (modelView.showModel)
            {
                gameManager.meshes[modelView.modelIndex].layers[modelView.layerIndex].SetActive(true);
                gameManager.CurrentMesh = gameManager.meshes[modelView.modelIndex].layers[modelView.layerIndex];
            }
        }
    }

    private void Update()
    {
        if (debugMode)
        {
            gameManager.mode = mode;
        }
    }
}
