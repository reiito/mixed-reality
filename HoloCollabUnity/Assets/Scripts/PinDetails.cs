﻿using UnityEngine;

[System.Serializable]
public class PinDetails
{
    public string id;
    public float[] pos;
    public float[] rot;

    public string comment;

    public bool display;

    public PinDetails()
    {
        pos = new float[3];
        rot = new float[4];
        comment = "";
    }

    public PinDetails(Transform newTransform)
    {
        pos = new float[3];
        rot = new float[4];
        SetTransform(newTransform);
        comment = "";
    }

    public PinDetails(Transform newTransform, string newComment)
    {
        pos = new float[3];
        rot = new float[4];
        SetTransform(newTransform);
        comment = newComment;
    }

    /// <summary>
    /// Translate given transform into float arrays (serializable)
    /// </summary>
    /// <param name="transform"></param>
    public void SetTransform(Transform transform)
    {
        pos[0] = transform.position.x;
        pos[1] = transform.position.y;
        pos[2] = transform.position.z;

        rot[0] = transform.rotation.x;
        rot[1] = transform.rotation.y;
        rot[2] = transform.rotation.z;
        rot[3] = transform.rotation.w;
    }

    /// <summary>
    /// Return pin's world location
    /// </summary>
    public Vector3 GetPosition()
    {
        return new Vector3(pos[0], pos[1], pos[2]);
    }

    /// <summary>
    /// Return pin's world rotation
    /// </summary>
    public Quaternion GetRotation()
    {
        return new Quaternion(rot[0], rot[1], rot[2], rot[3]);
    }
}