<?php

namespace App\Http\Controllers;

use App\Mesh;
use Illuminate\Http\Request;

class MeshController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Mesh::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return 'create';   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(Mesh::create($request->all()));   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mesh  $mesh
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if ($modelById = Mesh::find($id))
        {
            $model = $modelById;
        }
        elseif ($modelByName = Mesh::where('name' , '=', $id)->first()) 
        {
            $model = $modelByName;
        }
        else
        {
            return response()->json(['error' => 'No model found'], 404); // Status code here
        }
        
        // Unserialize the positional data
        $model->pins = unserialize($model->pins);
        return response()->json($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mesh  $mesh
     * @return \Illuminate\Http\Response
     */
    public function edit(Mesh $mesh)
    {
        return 'update';
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mesh  $mesh
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($modelById = Mesh::find($id))
        {
            $model = $modelById; 
        }
        elseif ($modelByName = Mesh::where('name' , '=', $id)->first()) 
        {
            $model = $modelByName; 
        }
        else
        {
            return response()->json(['error' => 'No model found'], 404); // Status code here
        } 
        
        // If a model was found and no error reached, update the model
        $model->comment = $request->input('comment');
        $model->pins = serialize($request->input('Pins'));
        $result = $model->save();
        
        // After modifying the object, return if success
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mesh  $mesh
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mesh $mesh)
    {
        return response()->json(Mesh::findOrFail($id));
    }
}
