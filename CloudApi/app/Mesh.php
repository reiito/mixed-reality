<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesh extends Model
{
    protected $fillable = [
        'name', 'pins',
    ];
}
